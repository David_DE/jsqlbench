package de.frost.david.java.jSQLBench.MVC.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JOptionPane;

public class QuitListener extends WindowAdapter implements ActionListener, ComponentListener {
	private CoreInterface c;

	public QuitListener(CoreInterface c) {
		this.c = c;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand() == "quit") {
			quit();
		}

	}

	@Override
	public void windowClosing(WindowEvent e) {
		quit();
	}

	private void quit() {
		int x = JOptionPane.showConfirmDialog(c.getFrame(), "Wirklich beenden?", "Beenden", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

		if (x == JOptionPane.YES_OPTION) {
			c.getFrame().setVisible(false);
			c.getFrame().dispose();
			System.exit(0);
		}
	}

	@Override
	public void componentResized(ComponentEvent e) {
		if ((c.getFrame().getWidth() > 800) && (c.getFrame().getWidth() < 1024)) {
			c.getSidebar().setWith(200);
		} else if (c.getFrame().getWidth() < 800) {
			c.getSidebar().setWith(150);
		} else if (c.getFrame().getWidth() > 1024) {
			c.getSidebar().setWith(250);
		}
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

}
