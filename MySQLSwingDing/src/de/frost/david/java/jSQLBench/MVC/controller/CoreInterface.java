package de.frost.david.java.jSQLBench.MVC.controller;

import javax.swing.JFrame;

import de.frost.david.java.jSQLBench.MVC.model.SQLModel;
import de.frost.david.java.jSQLBench.MVC.view.MBar;
import de.frost.david.java.jSQLBench.MVC.view.QueryZeile;
import de.frost.david.java.jSQLBench.MVC.view.SQLTableArea;
import de.frost.david.java.jSQLBench.MVC.view.Sidebar;

public interface CoreInterface {
	public JFrame getFrame();

	public Sidebar getSidebar();

	public MBar getMBar();

	public SQLTableArea getSQLTableArea();

	public void createSQL(String ip, String port, String db, String user, String pw);

	public SQLModel getSQL();

	public QueryZeile getQueryZeile();
}
