package de.frost.david.java.jSQLBench.MVC.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class SQLModel {
	private Connection c;
	private Statement s;
	private ResultSet r;
	private ResultSetMetaData rmd;
	private String url;
	private Vector<String[]> rows;
	private Vector<String> temp;

	private static SQLModel sql = null;

	private boolean time_error = false;

	private static String ip, port, db, user, pw;
	private boolean tables = false; // true = Database ausgewählt -> Tabellen
									// anzeigen

	public boolean isTables() {
		return tables;
	}

	public void setTables(boolean tables) {
		this.tables = tables;
	}

	private SQLModel(String ip, String port, String db, String user, String pw) {

		url = "jdbc:mysql://" + ip + ":" + port + "/" + db;// +"?zeroDateTimeBehavior=convertToNull";
		rows = new Vector<String[]>();
		temp = new Vector<String>();

		try {
			Class.forName("com.mysql.jdbc.Driver");
			if ((user == null) || (pw == null)) {
				c = DriverManager.getConnection(url);
			} else {
				c = DriverManager.getConnection(url, user, pw);
			}

		} catch (ClassNotFoundException e) {
			System.out.println("Klasse nicht gefunden!");

		} catch (SQLException e) {
			System.out.println("SQL Fehler beim Verbeinden!" + e.getSQLState());
		}

	}

	public static void construct(String ip, String port, String db, String user, String pw) {
		sql = new SQLModel(ip, port, db, user, pw);
	}

	public static SQLModel getSQLConnection(String ip, String port, String db, String user, String pw) {
		if (sql == null) {
			SQLModel.ip = ip;
			SQLModel.port = port;
			SQLModel.db = db;
			SQLModel.user = user;
			SQLModel.pw = pw;
			construct(ip, port, db, user, pw);
		} else if (SQLModel.ip != ip || SQLModel.port != port || SQLModel.db != db || SQLModel.user != user || SQLModel.pw != pw) {
			construct(ip, port, db, user, pw);
		}

		return sql;

	}

	public Vector<String[]> query(String q) throws Exception { // Exception
																// gegen
																// eigene
																// "SQL Exception austauschen"
		try {
			s = c.createStatement();
			r = s.executeQuery(q);
			rmd = r.getMetaData();
			int column = rmd.getColumnCount();
			rows.clear();

			while (r.next()) {
				String[] sa = new String[column];
				for (int x = 0; x < column; x++) {
					try {
						sa[x] = r.getString(x + 1); // Falls ein bestimmter Wert
													// einen Fehler wirft (z.B.
													// '0000-00-00 00:00:00')
					} catch (SQLException e) {
						sa[x] = "0000-00-00 00:00:00";

						if (!time_error) {
							// JOptionPane.showMessageDialog(null,
							// "Java kann 0000-00-00 00:00:00 nicht verarbeiten!",
							// "Fehler", JOptionPane.WARNING_MESSAGE);
							System.out.println("Error beim einlesen eines Wertes!\n" + e.getSQLState());
						}

						time_error = true;
					}
				}

				rows.add(sa);
			}

			Object[][] data = new Object[rows.size()][column];

			for (int x = 0; x < rows.size(); x++) {
				for (int y = 0; y < column; y++) {
					data[x][y] = rows.get(x)[y];
				}
			}

			return rows;

		} catch (SQLException e) {
			throw new Exception("SQL Fehler im Query" + e.getMessage());
		}
	}

	public Vector<String> queryDatensatz(String q) throws Exception { // Exception
																		// gegen
																		// eigene
																		// "SQL Exception austauschen"
		try {
			s = c.createStatement();
			r = s.executeQuery(q);
			temp.clear();

			while (r.next()) {
				temp.add(r.getString(1));
			}

			return temp;

		} catch (SQLException e) {
			throw new Exception("SQL Fehler in queryDatensatz");
		}
	}

	public Object[] getColumnNamesAsArray() throws Exception {
		try {
			int column = rmd.getColumnCount();

			Object[] spaltenNamen = new Object[column];

			for (int x = 0; x < column; x++) {
				spaltenNamen[x] = rmd.getColumnName(x + 1);

			}
			return spaltenNamen;

		} catch (SQLException e) {
			throw new Exception("SQL Fehler beim erstellen eines Arrays mit den Spaltennamen");
		}

	}

	public boolean queryUpdate(String q) {
		try {
			s = c.createStatement();
			s.executeUpdate(q);
			return true;
		} catch (SQLException e) {
			System.out.println("Fehler in queryUpdate: " + e.getErrorCode() + " / " + e.getMessage());
			return false;
		}
	}

	public String getColumnType(int col) {
		try {
			return rmd.getColumnTypeName(col);
		} catch (SQLException e) {
			e.printStackTrace();
			return "ERROR";
		}
	}

	public Vector<String[]> getData() {
		return rows;
	}

}
