package de.frost.david.java.jSQLBench.MVC.controller;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class MyTableModelListener implements TableModelListener {
	private CoreInterface c;

	public MyTableModelListener(CoreInterface c) {
		this.c = c;
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		boolean keinAND = true;
		int column = e.getColumn();
		int row = e.getFirstRow();
		if (column != -1) {
			String updateColumnName = c.getSQLTableArea().getTable().getColumnName(column);

			String update = "UPDATE " + c.getSidebar().getTableName() + " SET " + updateColumnName + " = '" + c.getSQLTableArea().getContent(row, column) + "' WHERE";

			for (int x = 0; x < c.getSQLTableArea().getTable().getColumnCount(); x++) {

				if (x != column) {
					if (!keinAND) {
						update += " AND";
					} else {
						keinAND = false;
					}
					if (c.getSQL().getColumnType(x + 1).equals("INT")) {
						update += (" " + c.getSQLTableArea().getTable().getColumnName(x) + " = " + c.getSQLTableArea().getContent(row, x));
					} else {
						update += (" " + c.getSQLTableArea().getTable().getColumnName(x) + " = '" + c.getSQLTableArea().getContent(row, x) + "'");
					}

				}
			}
			update += ";";
			System.out.println(update);

			if (!c.getSQL().queryUpdate(update)) {
				c.getSQLTableArea().reload();
			}

		}

	}

}
