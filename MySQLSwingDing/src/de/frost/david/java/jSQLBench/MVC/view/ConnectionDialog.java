package de.frost.david.java.jSQLBench.MVC.view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.frost.david.java.jSQLBench.MVC.controller.CoreInterface;

@SuppressWarnings("serial")
public class ConnectionDialog extends JDialog implements ActionListener {
	private JButton btn_connect, btn_cancel;
	private JTextField tf_ip, tf_port, tf_db, tf_user, tf_pw;
	private JPanel jp1, jp2, jp3, jp4, jp5, jp6;

	private CoreInterface c;

	public ConnectionDialog(CoreInterface c) {
		this.c = c;
		setModal(true);
		setTitle("Connect");
		setSize(350, 175);
		this.setLocationRelativeTo(c.getFrame());
		this.setLayout(new FlowLayout());
		this.setResizable(false);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		btn_connect = new JButton("Verbinden");
		btn_connect.addActionListener(this);
		btn_connect.setActionCommand("con");
		btn_cancel = new JButton("Abbrechen");
		btn_cancel.addActionListener(this);

		jp1 = new JPanel();
		jp1.add(new JLabel("IP: "));
		tf_ip = new JTextField(10);
		jp1.add(tf_ip);
		this.add(jp1);

		jp2 = new JPanel();
		jp2.add(new JLabel("Port: "));
		tf_port = new JTextField(10);
		jp2.add(tf_port);
		this.add(jp2);

		jp3 = new JPanel();
		jp3.add(new JLabel("DB: "));
		tf_db = new JTextField(10);
		jp3.add(tf_db);
		this.add(jp3);

		jp4 = new JPanel();
		jp4.add(new JLabel("User: "));
		tf_user = new JTextField(10);
		jp4.add(tf_user);
		this.add(jp4);

		jp5 = new JPanel();
		jp5.add(new JLabel("PW: "));
		tf_pw = new JTextField(10);
		jp5.add(tf_pw);
		this.add(jp5);

		jp6 = new JPanel();
		jp6.add(btn_connect);
		jp6.add(btn_cancel);
		this.add(jp6);

		tf_ip.setText("localhost");
		tf_port.setText("3306");
		tf_db.setText("spieleserver");
		tf_user.setText("root");
		tf_pw.setText("");

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "con") {
			c.createSQL(this.tf_ip.getText(), this.tf_port.getText(), this.tf_db.getText(), this.tf_user.getText(), this.tf_pw.getText());
			try {

				if (this.tf_db.getText().trim().length() == 0) {
					c.getSidebar().setContent(c.getSQL().queryDatensatz("SHOW DATABASES;"));
					c.getSidebar().setLabel("Databases");
				} else {
					c.getSidebar().setContent(c.getSQL().queryDatensatz("SHOW TABLES;"));
					c.getSidebar().setLabel(this.tf_db.getText() + "/Tables");
					c.getSidebar().getModel().add(0, "(...)");
					c.getSQL().setTables(true);
				}

				c.getQueryZeile().setTF(true);

			} catch (Exception e1) {
				System.out.println("Fehler beim Verbinden! in ConnectionDialog->actionPerformed");
			}
		}
		this.setVisible(false);
	}

}
