package de.frost.david.java.jSQLBench.MVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.frost.david.java.jSQLBench.MVC.controller.CoreInterface;

@SuppressWarnings("serial")
public class QueryZeile extends JPanel implements ActionListener {
	private JLabel label;
	private JTextField tf;
	private CoreInterface c;

	public QueryZeile(CoreInterface c) {
		this.c = c;
		label = new JLabel("SELECT");
		tf = new JTextField();
		tf.addActionListener(this);
		tf.setColumns(50);
		// tf.setEnabled(false);
		// tf.setText("DISABLED");

		add(label);
		add(tf);
	}

	public void setTF(boolean v) {
		this.tf.setEditable(v);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Vector<String[]> s;
		try {
			s = c.getSQL().query("SELECT " + tf.getText());
			Object[] colnam = c.getSQL().getColumnNamesAsArray();
			c.getSQLTableArea().fill(colnam, s);
		} catch (Exception e1) {
			System.out.println("Error in der Query Zeile" + e1.getMessage());
		}

	}

}
