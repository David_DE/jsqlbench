package de.frost.david.java.jSQLBench.MVC.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.JFrame;

import de.frost.david.java.jSQLBench.MVC.controller.CoreInterface;
import de.frost.david.java.jSQLBench.MVC.controller.QuitListener;
import de.frost.david.java.jSQLBench.MVC.model.SQLModel;

@SuppressWarnings("serial")
public class JBench extends JFrame {
	private static final int HEIGHT = 510;
	private static final int WIDTH = 640;
	private CoreInterface c;
	private JFrame frame;
	private SQLTableArea area;
	private Sidebar sb;
	private MBar bar;
	private QueryZeile qZeile;
	private SQLModel sql;

	public JBench() {
		c = new Core();
		frame = this;
		this.setTitle("jSQLBench - MySQL - v0.2.1");
		setSize(WIDTH, HEIGHT);
		setMinimumSize(new Dimension(WIDTH, HEIGHT));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		QuitListener ql = new QuitListener(c);
		this.addWindowListener(ql);
		this.addComponentListener(ql);

		Container cp = getContentPane();
		cp.setLayout(new BorderLayout(5, 5));

		area = new SQLTableArea(c);
		sb = new Sidebar(c);

		cp.add(area, BorderLayout.CENTER);
		cp.add(sb, BorderLayout.WEST);

		// try {
		// Object[][] data = sql.query(q2);
		// Object[] spaltenNamen = sql.getColumnNamesAsArray();
		//
		// area = new SQLTableArea(data, spaltenNamen);
		// sb = new Sidebar(sql.queryDatensatz("SHOW DATABASES;"));
		//
		// cp.add(area, BorderLayout.CENTER);
		// cp.add(sb, BorderLayout.WEST);
		//
		// } catch (Exception e) {
		// System.out.println("Fehler im Main try"+e.getMessage());
		// }

		bar = new MBar(c);
		cp.add(bar, BorderLayout.NORTH);
		qZeile = new QueryZeile(c);
		cp.add(qZeile, BorderLayout.SOUTH);

		setVisible(true);

	}

	public class Core implements CoreInterface {

		public JFrame getFrame() {
			return frame;
		}

		public Sidebar getSidebar() {
			return sb;
		}

		public MBar getMBar() {
			return bar;
		}

		public SQLTableArea getSQLTableArea() {
			return area;
		}

		public void createSQL(String ip, String port, String db, String user, String pw) {
			sql = SQLModel.getSQLConnection(ip, port, db, user, pw);
		}

		public SQLModel getSQL() {
			return sql;
		}

		public QueryZeile getQueryZeile() {
			return qZeile;
		}

	}

}
