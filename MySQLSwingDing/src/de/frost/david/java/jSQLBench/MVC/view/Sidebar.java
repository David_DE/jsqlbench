package de.frost.david.java.jSQLBench.MVC.view;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import de.frost.david.java.jSQLBench.MVC.controller.CoreInterface;

@SuppressWarnings("serial")
public class Sidebar extends JPanel {
	private JList<String> list;
	private JScrollPane scroll;
	private JLabel label;
	private String tableName;

	private CoreInterface c;

	private DefaultListModel<String> model;

	public Sidebar(CoreInterface c) {
		this.c = c;
		model = new DefaultListModel<String>();
		list = new JList<String>(model);
		this.setPreferredSize(new Dimension(150, 10));

		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		label = new JLabel("Tables:");
		this.add(label);

		scroll = new JScrollPane();
		scroll.add(list);
		scroll.setViewportView(list);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		this.add(scroll);

		list.addMouseListener(new MyMouseListener());

	}

	public DefaultListModel<String> getModel() {
		return model;
	}

	public void setLabel(String s) {
		label.setText(s);
	}

	public void setContent(Vector<String> content) {
		model.removeAllElements();

		Iterator<String> i = content.iterator();

		while (i.hasNext()) {
			model.addElement(i.next());
		}

	}

	public void setWith(int i) {
		this.setPreferredSize(new Dimension(i, 10));
	}

	public String getTableName() {
		return tableName;
	}

	public class MyMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

			if (!list.getSelectedValue().equals("(...)")) {
				if (c.getSQL().isTables()) {
					Vector<String[]> data;
					try {
						tableName = list.getSelectedValue();
						data = c.getSQL().query("SELECT * FROM " + tableName + ";");
						Object[] spaltenNamen = c.getSQL().getColumnNamesAsArray();
						c.getSQLTableArea().fill(spaltenNamen, data);
						// c.getSQLTableArea().selectCell(3, 3);
						c.getMBar().enableItem("such");
					} catch (Exception e1) { // gegen eigene Exception Tauschen
						System.out.println("Fehler in mouseClicked");
					}
				} else { // database ist noch nicht ausgew�hlt

					try {
						String s = list.getSelectedValue();
						c.getSQL().queryUpdate("USE " + s + ";");
						c.getSidebar().setContent(c.getSQL().queryDatensatz("SHOW TABLES;"));
						c.getSQL().setTables(true);
						c.getSidebar().setLabel(s + "/Tables");
						model.add(0, "(...)");
					} catch (Exception e1) { // gegen eigene Exception Tauschen
						System.out.println("Fehler in mouseClicked");
					}
				}

			} else { // (...) -> Zur�ck
				try {
					c.getSidebar().setContent(c.getSQL().queryDatensatz("SHOW DATABASES;"));
					c.getSQL().setTables(false);
					c.getSidebar().setLabel("Databases");
				} catch (Exception e1) {
					System.out.println("Fehler in mouseClicked -> else -> (...)");
				}
			}

		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

	}

}
