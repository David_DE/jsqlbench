package de.frost.david.java.jSQLBench.MVC.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import de.frost.david.java.jSQLBench.MVC.controller.CoreInterface;
import de.frost.david.java.jSQLBench.MVC.controller.MenuActionListener;
import de.frost.david.java.jSQLBench.MVC.controller.QuitListener;

@SuppressWarnings("serial")
public class MBar extends JMenuBar {
	private CoreInterface c;

	private JMenu file;
	private JMenu edit;

	private JMenuItem file_connect, file_quit, edit_search, edit_search_cont;

	private ConnectionDialog cd;

	public MBar(CoreInterface c) {
		this.c = c;

		cd = new ConnectionDialog(c);

		file = new JMenu("Datei");
		edit = new JMenu("Bearbeiten");

		file_connect = new JMenuItem("Connect");

		file_connect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cd.setVisible(true);
			}
		});

		file_quit = new JMenuItem("Beenden");
		file_quit.setActionCommand("quit");
		file_quit.addActionListener(new QuitListener(c));

		edit_search = new JMenuItem("Suchen");
		edit_search.setActionCommand("such");
		edit_search.addActionListener(new MenuActionListener(c));
		edit_search.setEnabled(false);

		edit_search_cont = new JMenuItem("Weitersuchen");
		edit_search_cont.setActionCommand("wsuch");
		edit_search_cont.addActionListener(new MenuActionListener(c));
		edit_search_cont.setEnabled(false);

		file.add(file_connect);
		file.add(file_quit);

		edit.add(edit_search);
		edit.add(edit_search_cont);

		add(file);
		add(edit);
	}

	public void enableItem(String item) {
		if (item.equals("such")) {
			edit_search.setEnabled(true);
		} else if (item.equals("wsuch")) {
			edit_search_cont.setEnabled(true);
		}
	}

}
