package de.frost.david.java.jSQLBench.MVC.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class MenuActionListener implements ActionListener {
	private CoreInterface c;

	public MenuActionListener(CoreInterface c) {
		this.c = c;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("such")) {
			c.getSQLTableArea().suchen(JOptionPane.showInputDialog(c.getFrame(), "Bitte Suchbegriff eingeben", "Suche", JOptionPane.PLAIN_MESSAGE));
		} else if (e.getActionCommand().equals("wsuch")) {
			c.getSQLTableArea().weitersuchen();
		}

	}

}
