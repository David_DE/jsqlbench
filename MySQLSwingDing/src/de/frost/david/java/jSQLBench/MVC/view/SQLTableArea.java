package de.frost.david.java.jSQLBench.MVC.view;

import java.awt.BorderLayout;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import de.frost.david.java.jSQLBench.MVC.controller.CoreInterface;
import de.frost.david.java.jSQLBench.MVC.controller.MyTableModelListener;

@SuppressWarnings("serial")
public class SQLTableArea extends JPanel {
	private JTable table;
	private JScrollPane scroll;
	private DefaultTableModel model;
	private CoreInterface c;

	private String gesucht;

	public SQLTableArea(CoreInterface c) {
		this.c = c;
		model = new DefaultTableModel();
		table = new JTable(model);
		scroll = new JScrollPane();
		this.setLayout(new BorderLayout());
		table.setCellSelectionEnabled(true);

		scroll.add(table);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scroll.setBorder(BorderFactory.createEmptyBorder());
		scroll.setViewportView(table);
		this.add(scroll, BorderLayout.CENTER);

		model.addTableModelListener(new MyTableModelListener(c));
	}

	public DefaultTableModel getTableModel() {
		return this.model;
	}

	public JTable getTable() {
		return this.table;
	}

	public void fill(Object[] spaltenname, Vector<String[]> data) {
		this.model.setColumnCount(0); // -> Spalten l�schen
		this.model.setRowCount(0); // -> Zeilen l�schen

		for (int x = 0; x < spaltenname.length; x++) {
			this.model.addColumn((String) spaltenname[x]);
		}

		Iterator<String[]> i = data.iterator();

		while (i.hasNext()) {
			this.model.addRow(i.next());
		}
	}

	public void suchen(String s) {
		Vector<Vector<String>> vec = model.getDataVector();
		gesucht = s;
		c.getMBar().enableItem("wsuch");

		for (int x = 0; x < vec.size(); x++) {
			Vector<String> vec2 = vec.get(x);

			for (int y = 0; y < vec2.size(); y++) {
				if (vec2.get(y).indexOf(s) != -1) {
					selectCell(x, y);
					return;
				}
			}

		}
	}

	public void weitersuchen() {
		if (gesucht != null) {
			Vector<Vector<String>> vec = model.getDataVector();
			int z = 0;

			for (int x = table.getSelectedRow(); x < vec.size(); x++) {
				Vector<String> vec2 = vec.get(x);
				System.out.println("X: " + x);

				if (x == table.getSelectedRow()) {
					z = table.getSelectedColumn() + 1;
				} else {
					z = 0;
				}

				for (int y = z; y < vec2.size(); y++) {
					System.out.println("Y: " + y);
					if (vec2.get(y).indexOf(gesucht) != -1) {
						selectCell(x, y);
						return;
					}
				}

			}
		}

	}

	public void selectCell(int r, int c) {
		if ((model.getRowCount() > r) && (model.getColumnCount() > c)) {
			table.setRowSelectionInterval(r, r);
			table.setColumnSelectionInterval(c, c);
		}
	}

	public String getContent(int row, int col) {
		Vector<Vector<String>> vec = model.getDataVector();
		return vec.get(row).get(col);
	}

	public void reload() {
		try {
			fill(c.getSQL().getColumnNamesAsArray(), c.getSQL().getData());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Fehler in SQLTableArea->reload()");
		}
	}

}
